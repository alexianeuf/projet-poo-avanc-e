/*
 *  Freeplane - mind map editor
 *  Copyright (C) 2009 Dimitry
 *
 *  This file author is Dimitry
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.freeplane.core.ui.components;

import org.freeplane.core.ui.AFreeplaneAction;
import org.freeplane.core.ui.IAcceleratorChangeListener;
import org.freeplane.core.ui.IKeyStrokeProcessor;
import org.freeplane.core.ui.SetAcceleratorOnNextClickAction;
import org.freeplane.core.util.TextUtils;
import org.freeplane.features.icon.factory.IconStoreFactory;
import org.freeplane.features.mode.Controller;
import org.freeplane.features.mode.mindmapmode.MModeController;

import javax.swing.*;
import javax.swing.text.JTextComponent;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.freeplane.core.ui.svgicons.FreeplaneIconFactory.toImageIcon;

/**
 * @author Dimitry Polivaev
 * 03.07.2009
 */
public class FButtonBar extends JComponent implements IAcceleratorChangeListener, KeyEventDispatcher,
        WindowFocusListener, IKeyStrokeProcessor {
    /**
     * Le font des boutons qui seront créés (couleurs, bordures etc ...)
     */
    private static final Font BUTTON_FONT = new JButton().getFont().deriveFont(UITools.getUIFontSize(1.1));

    /**
     * Le nombre de boutons maximum dans la liste
     */
    private static final int BUTTON_NUMBER = 12;

    /**
     * UID pour la serialisation
     */
    private static final long serialVersionUID = 1L;

    /**
     * Map contenant les listes de boutons avec pour chaque liste un ID
     */
    final private Map<Integer, JButton[]> buttons;
    final private Timer timer;
    private int lastModifiers = -1;
    private int nextModifiers = 0;

    /**
     * Le parent de la barre de boutons
     */
    private JFrame ownWindowAncestor;
    private boolean altPressedEventHidden = false;

    /**
     * Constructeur par défaut
     */
    public FButtonBar() {
        timer = new Timer(500, e -> onModifierChangeImpl());
        timer.setRepeats(false);
        KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(this);
        buttons = new HashMap<>();
        onModifierChange();
    }

    /**
     * Fonction pour le changement de l'accelerator
     *
     * @param action    L'action
     * @param oldStroke L'ancien keyStroke
     * @param newStroke Le nouveau keyStroke
     */
    @Override
    public void acceleratorChanged(final AFreeplaneAction action, final KeyStroke oldStroke, final KeyStroke newStroke) {
        final int oldButtonNumber = oldStroke != null ? oldStroke.getKeyCode() - KeyEvent.VK_F1 : -1;
        final int newButtonNumber = newStroke != null ? newStroke.getKeyCode() - KeyEvent.VK_F1 : -1;
        if (oldButtonNumber >= 0 && oldButtonNumber < BUTTON_NUMBER) {
            final int modifiers = oldStroke.getModifiers()
                    & (KeyEvent.CTRL_MASK | KeyEvent.META_MASK | KeyEvent.SHIFT_MASK | KeyEvent.ALT_MASK | KeyEvent.ALT_GRAPH_MASK);
            final JButton[] buttonRow = buttons.get(modifiers);
            final JButton button = buttonRow[oldButtonNumber];
            setAcceleratorAction(button, oldStroke);
        }
        if (newButtonNumber >= 0 && newButtonNumber < BUTTON_NUMBER) {
            final int modifiers = newStroke.getModifiers()
                    & (KeyEvent.CTRL_MASK | KeyEvent.META_MASK | KeyEvent.SHIFT_MASK | KeyEvent.ALT_MASK | KeyEvent.ALT_GRAPH_MASK);
            final JButton[] buttonRow = createButtons(modifiers);
            final JButton button = buttonRow[newButtonNumber];
            final String text = (String) action.getValue(Action.NAME);
            button.setText(text);
            button.setToolTipText(text);
            button.setAction(action);
            button.setEnabled(action.isEnabled());
        }
    }

    /**
     * Fonction pour set l'action de l'accelerator
     *
     * @param button Le bouton auquel on va associer l'action
     * @param ks     Le keyStroke auquel on va associer l'action
     */
    private void setAcceleratorAction(final JButton button, final KeyStroke ks) {
        final Action setAcceleratorAction = new SetFKeyAcceleratorOnNextClickAction(ks);
        button.setAction(setAcceleratorAction);
        button.setEnabled(setAcceleratorAction.isEnabled());
        final String text = TextUtils.getText("f_button_unassigned");
        button.setText(text);
    }

    /**
     * Fonction pour clean les modifiers
     *
     * @param modifiers L'int des modifiers
     */
    private void cleanModifiers(final int modifiers) {
        if ((nextModifiers & modifiers) == 0) {
            return;
        }
        nextModifiers &= ~modifiers;
        onModifierChange();
    }

    /**
     * Fonction pour créer une ligne de bouton selon le modifiers
     *
     * @param modifiers L'int des modifiers
     * @return La liste des boutons de la barre
     */
    private JButton[] createButtonRow(final int modifiers) {
        final JButton[] buttons = new JButton[BUTTON_NUMBER];
        for (int i = 0; i < BUTTON_NUMBER; i++) {
            final String name = "f" + (i + 1) + ".png";
            final JButton button = buttons[i] = new JButton(toImageIcon(IconStoreFactory.ICON_STORE.getUIIcon(name).getIcon())) {
                private static final long serialVersionUID = 1L;

                @Override
                protected void configurePropertiesFromAction(final Action a) {
                }
            };
            button.setFont(BUTTON_FONT);
            button.setFocusable(false);
            button.setBorder(BorderFactory.createEtchedBorder());
            if (System.getProperty("os.name").startsWith("Mac OS")) {
                button.setBorderPainted(false);
            }
            KeyStroke ks = KeyStroke.getKeyStroke(KeyEvent.VK_F1 + i, modifiers);
            setAcceleratorAction(button, ks);

        }
        return buttons;
    }

    /**
     * Fonction de création de boutons à partir de l'int des modifiers
     *
     * @param modifiers l'int des modifiers
     * @return La liste des boutons
     */
    private JButton[] createButtons(final int modifiers) {
        JButton[] buttonRow = buttons.get(modifiers);
        buttonRow = buttonRow != null ? buttonRow : createButtonRow(modifiers);
        buttons.put(modifiers, buttonRow);
        return buttonRow;
    }

    /**
     * Fonction permettant de dispatcher (rediriger) l'event en paramètre
     *
     * @param e L'event a redirigier
     * @return Un booléen ?
     */
    @Override
    public boolean dispatchKeyEvent(final KeyEvent e) {
        if (!(Controller.getCurrentModeController() instanceof MModeController))
            return false;
        ownWindowAncestor = ownWindowAncestor != null ? ownWindowAncestor : (JFrame) SwingUtilities.getWindowAncestor(this);
        if (ownWindowAncestor == null)
            return false;
        ownWindowAncestor.addWindowFocusListener(this);

        final Window windowAncestor = SwingUtilities.getWindowAncestor(e.getComponent());

        if (windowAncestor == ownWindowAncestor && ownWindowAncestor.getJMenuBar() != null && ownWindowAncestor.getJMenuBar().isEnabled()) {
            processDispatchedKeyEvent(e);
        } else {
            resetModifiers();
        }
        if (e.getKeyCode() == KeyEvent.VK_ALT) {
            if (e.getID() == KeyEvent.KEY_PRESSED) {
                final Component focusOwner = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
                return altPressedEventHidden = !(focusOwner instanceof JRootPane || 0 == (e.getModifiersEx() & ~(KeyEvent.ALT_MASK | KeyEvent.ALT_DOWN_MASK)));
            } else {
                if (altPressedEventHidden) {
                    altPressedEventHidden = false;
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Fonction lancée lors d'un changement de modifier
     */
    private void onModifierChange() {
        if (lastModifiers == nextModifiers) {
            return;
        }
        if (timer.isRunning()) {
            timer.stop();
        }
        if (nextModifiers == 0) {
            onModifierChangeImpl();
        } else {
            timer.start();
        }
    }

    private void onModifierChangeImpl() {
        if (lastModifiers == nextModifiers) {
            return;
        }
        lastModifiers = nextModifiers;
        removeAll();
        final JButton[] buttonRow = createButtons(nextModifiers);
        for (final JButton button : buttonRow) {
            add(button);
        }
        revalidate();
        repaint();
    }

    /**
     * Fonction pour gérer les évent dispatchés
     *
     * @param e L'évent dispatché
     */
    private void processDispatchedKeyEvent(final KeyEvent e) {
        final int keyCode = e.getKeyCode();
        final int eID = e.getID();

        /*
        On clean ou set les modifiers selon le keycode et le keyevent
         */
        switch (keyCode) {
            case KeyEvent.VK_CONTROL:
                if (eID == KeyEvent.KEY_RELEASED)
                    cleanModifiers(KeyEvent.CTRL_MASK);
                else
                    setModifiers(KeyEvent.CTRL_MASK);
                break;
            case KeyEvent.VK_META:
                if (eID == KeyEvent.KEY_RELEASED)
                    cleanModifiers(KeyEvent.META_MASK);
                else
                    setModifiers(KeyEvent.META_MASK);
                break;
            case KeyEvent.VK_SHIFT:
                if (eID == KeyEvent.KEY_RELEASED)
                    cleanModifiers(KeyEvent.SHIFT_MASK);
                else
                    setModifiers(KeyEvent.SHIFT_MASK);
                break;
            case KeyEvent.VK_ALT:
                if (eID == KeyEvent.KEY_RELEASED)
                    cleanModifiers(KeyEvent.ALT_MASK);
                else
                    setModifiers(KeyEvent.ALT_MASK);
                break;
            case KeyEvent.VK_ALT_GRAPH:
                if (eID == KeyEvent.KEY_RELEASED)
                    cleanModifiers(KeyEvent.ALT_GRAPH_MASK);
                else
                    setModifiers(KeyEvent.ALT_GRAPH_MASK);
                break;
        }
    }

    @Override
    public boolean processKeyBinding(KeyStroke ks, KeyEvent e) {
        return processFKey(e);
    }

    private boolean processFKey(final KeyEvent e) {
        if (e.getID() != KeyEvent.KEY_PRESSED)
            return false;
        final Window windowAncestor = SwingUtilities.getWindowAncestor(e.getComponent());
        if (windowAncestor != ownWindowAncestor) {
            resetModifiers();
            return false;
        }
        int keyCode = e.getKeyCode();
        if (keyCode >= KeyEvent.VK_F1 && keyCode <= KeyEvent.VK_F12) {
            final JButton btn = Arrays.asList(createButtons(nextModifiers)).get(keyCode - KeyEvent.VK_F1);
            if (btn.getAction() instanceof SetAcceleratorOnNextClickAction
                    && e.getComponent() instanceof JTextComponent)
                return false;
            if (timer.isRunning()) {
                timer.stop();
                onModifierChangeImpl();
            }
            btn.doClick();
            return true;
        }
        return false;
    }

    /**
     * Fonction pour remettre à zéro le modifiers
     */
    private void resetModifiers() {
        if (nextModifiers == 0) {
            return;
        }
        nextModifiers = 0;
        onModifierChange();
    }

    /**
     * FOnction pour set les modifiers
     * @param modifiers L'int des modifiers
     */
    private void setModifiers(final int modifiers) {
        if ((modifiers & ~nextModifiers) == 0) {
            return;
        }
        nextModifiers |= modifiers;
        onModifierChange();
    }

    @Override
    public void windowGainedFocus(final WindowEvent e) {
    }

    @Override
    public void windowLostFocus(final WindowEvent e) {
        resetModifiers();
    }

    @Override
    public void layout() {
        final int width = getParent().getWidth();
        final int border = 5;
        final int height = getComponent(1).getPreferredSize().height;
        final int componentCount = getComponentCount();
        final float availableWidth = width - 2 * border + 0f;
        final float dw = availableWidth / componentCount;
        int preferredWidth = 0;
        int narrowComponentPreferredWidth = 0;
        for (int i = 0; i < componentCount; i++) {
            final int cw = getComponent(i).getPreferredSize().width;
            preferredWidth += cw;
            if (cw <= dw) {
                narrowComponentPreferredWidth += cw;
            }
        }
        final float k;
        if (availableWidth < preferredWidth) {
            k = (availableWidth - narrowComponentPreferredWidth) / (preferredWidth - narrowComponentPreferredWidth);
        } else {
            k = availableWidth / preferredWidth;
        }
        float x = border;
        for (int i = 0; i < componentCount; i++) {
            float cw = getComponent(i).getPreferredSize().width;
            if (k > 1f || cw > dw) {
                cw *= k;
            }
            getComponent(i).setBounds((int) x, 0, (int) cw, height);
            x += cw;
        }
    }

    /**
     * Fonction pour récupérer les dimensions précédemment enregistrées
     *
     * @return Les dimensions
     */
    @Override
    public Dimension getPreferredSize() {
        return new Dimension(getParent().getWidth(), getComponent(1).getPreferredSize().height);
    }
}
