/*
 *  Freeplane - mind map editor
 *  Copyright (C) 2008 Joerg Mueller, Daniel Polansky, Christian Foltin, Dimitry Polivaev
 *
 *  This file is modified by Dimitry Polivaev in 2008.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.freeplane.core.ui.components;

import org.freeplane.core.ui.IKeyStrokeProcessor;

import javax.swing.*;
import javax.swing.text.JTextComponent;
import java.awt.event.KeyEvent;

/**
 * This is the menu bar for Freeplane. Actions are defined in MenuListener.
 * Moreover, the StructuredMenuHolder of all menus are hold here.
 */
public class FreeplaneMenuBar extends JMenuBar {
    /**
     * Pour la sérialisation
     */
    private static final long serialVersionUID = 1L;

    /**
     * Le KeyStrokeProcessor pour les liaisons des events
     */
    final private IKeyStrokeProcessor keyStrokeProcessor;

    /**
     * Les modifiers
     */
    static final int KEY_MODIFIERS = KeyEvent.SHIFT_DOWN_MASK | KeyEvent.SHIFT_MASK | KeyEvent.ALT_GRAPH_DOWN_MASK | KeyEvent.ALT_GRAPH_MASK;

    /**
     * Constructeur prenant en paramètre le processor
     *
     * @param keyStrokeProcessor Le KeyStrokeProcessor
     */
    public FreeplaneMenuBar(IKeyStrokeProcessor keyStrokeProcessor) {
        this.keyStrokeProcessor = keyStrokeProcessor;
        getInputMap(WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F10, 0), "none");
    }

    /**
     * @param keyStroke Le keyStroke qui sera modifié et retourné
     * @param keyChar   Le charactère à partir duquel on va potentiellement récupérer un nouveau keyStroke
     * @return Le keytroke
     */
    public static KeyStroke derive(final KeyStroke keyStroke, final Character keyChar) {
        if (keyStroke == null) return null;
        final int modifiers = keyStroke.getModifiers();
        if (keyStroke.getKeyChar() != KeyEvent.CHAR_UNDEFINED) return keyStroke;
        if ((modifiers & KEY_MODIFIERS) != 0) {
            KeyStroke.getKeyStroke(KeyEvent.getExtendedKeyCodeForChar(keyChar), modifiers & ~KEY_MODIFIERS, keyStroke.isOnKeyRelease());
        }
        if (keyChar != '\0' && keyChar != KeyEvent.CHAR_UNDEFINED) {
            return KeyStroke.getKeyStroke(keyChar, modifiers);
        }
        return keyStroke;
    }

    /**
     * Fonction pour gérer les liaisons des évents avec les key (touches)
     *
     * @param keyStroke Le KeyStroke
     * @param event     L'event
     * @param condition
     * @param pressed   Si la touche a été pressée (pressed, released ou autre)
     * @return Un booléen ?
     */
    @Override
    public boolean processKeyBinding(final KeyStroke keyStroke, final KeyEvent event, final int condition, final boolean pressed) {
        if (event.getKeyChar() != KeyEvent.CHAR_UNDEFINED && event.getKeyChar() != '\0' && event.getKeyChar() != KeyEvent.VK_ESCAPE
                && 0 == (event.getModifiers() & ~KEY_MODIFIERS) && event.getSource() instanceof JTextComponent) {
            return false;
        }
        if (keyStrokeProcessor.processKeyBinding(keyStroke, event) || super.processKeyBinding(keyStroke, event, condition, pressed)) {
            return true;
        }
        final KeyStroke derivedKS = FreeplaneMenuBar.derive(keyStroke, event.getKeyChar());
        if (derivedKS == keyStroke) {
            return false;
        }
        return super.processKeyBinding(derivedKS, event, condition, pressed);
    }
}
