package org.freeplane.core.ui.menubuilders.generic;

import java.util.*;

/**
 * @author Dimitry
 *
 */
public class Entry {

    /**
     * The entry name
     */
	private String name;
    /**
     * The entry parent
     */
	private Entry parent;
    /**
     * The entry builders
     */
	private List<String> builders;
    /**
     * The entry attributs
     */
	final private Map<Object, Object> attributes;
    /**
     * The entry children
     */
	final private ArrayList<Entry> childEntries;


    /**
     * Contructor.
     */
	public Entry() {
		super();
		this.name = "";
		childEntries = new ArrayList<Entry>();
		attributes = new HashMap<Object, Object>();
		builders = Collections.emptyList();
	}


    /**
     * Set the attribute value for a given key
     * @param key final string the key of the attribute
     * @param value Object the value of the attribute
     */
	public void setAttribute(final String key, Object value) {
		setAttributeObject(key, value);
	}

    /**
     * Set the attribute value for a given class
     * @param valueClass Class the class of the attribute
     * @param value Object the value of the attribute
     */
	public void setAttribute(Class<?> valueClass, Object value) {
		setAttributeObject(valueClass, value);
	}

    /**
     * Set the Object attribute value for a given key
     * @param key final Object key of the Object attribute
     * @param value Object value of the Object attribute
     */
	private void setAttributeObject(final Object key, Object value) {
		if(attributes.containsKey(key)){
			if(value != attributes.get(key)) {
				throw new AttributeAlreadySetException(this, key, attributes.get(key));
			}
		}
		else
			attributes.put(key, value);
	}

    /**
     * Get the attribute value for a given key
     * @param key final String the key of the attribute to get
     * @return an Object containing the value of the attribute
     */
	public Object getAttribute(final String key) {
		return attributes.get(key);
	}

    /**
     * Get the attribute for a given key
     * @param key final Class the key of the attribute
     * @param <T> the type of the attribute
     * @return the value of the attribute
     */
	@SuppressWarnings("unchecked")
	public <T> T getAttribute(final Class<T> key) {
		return (T)attributes.get(key);
	}

    /**
     * Add a child to the current entry
     * @param entry the child to add
     */
	public void addChild(Entry entry) {
		childEntries.add(entry);
		entry.setParent(this);
	}

    /**
     * Set the parent of the current entry
     * @param parent the parent to set
     */
	private void setParent(Entry parent) {
		this.parent = parent;

	}

    /**
     * Set the builders of the current entry
     * @param builders List the builders list to set
     * @return the current object
     */
	public Entry setBuilders(List<String> builders) {
		this.builders = builders;
		return this;

	}

    /**
     * Set the builders of the current object
     * @param builders Array the builders to set
     * @return the result of the function setBuilders()
     */
	public Entry setBuilders(String... builders) {
		return setBuilders(Arrays.asList(builders));
	}

    /**
     * Get the parent of the current object
     * @return an Entry object representing the parent
     */
	public Entry getParent() {
		return parent;
	}

    /**
     * Set the entry name
     * @param name String the value of the name
     */
	public void setName(String name) {
		this.name = name;
	}

    /**
     * Get the entry path
     * @return a String object containing the path
     */
	public String getPath() {
		return (parent != null ? parent.getPath() : "") +  "/" + getName();
	}

    /**
     * Get the current entry's name
     * @return a String object representing the name
     */
	public String getName() {
		return name;
	}

    /**
     * Get the child at a given index
     * @param index int the child position in list
     * @return an Entry object representing the child asked | null if don't exist
     */
	public Entry getChild(int index) {
		return childEntries.get(index);
	}

    /**
     * Get the lowest child for the indices given
     * @param indices Array the indices to get
     * @return the lowest child in the tree
     */
	public Entry getChild(int... indices) {
		Entry entry = this;
		for(int index : indices)
			entry = entry.getChild(index);
		return entry;
	}


    /**
     * Get the entry's children
     * @return a list of all the children of the entry
     */
	public List<Entry> children() {
		return childEntries;
	}


    /**
     * Get the entry builders
     * @return a collection of String containing all the builders
     */
	public Collection<String> builders() {
		return builders;
	}


    /**
     * Erase every children for the current entry
     */
	public void removeChildren() {
		childEntries.clear();
	}

    /**
     * Remove the attribute for a given key
     * @param key String the key of the attribute to remove
     * @return the previous value or null
     */
	public Object removeAttribute(String key) {
		return attributes.remove(key);
	}

    /**
     * Remove the attribute for a given key
     * @param valueClass Class the key of the attribute to remove
     * @return the previous value or null
     */
	@SuppressWarnings("unchecked")
	public <T> T removeAttribute(Class<T> valueClass) {
		return (T) attributes.remove(valueClass);
	}

    /**
     * Indicate if has children
     * @return true if as children else false
     */
	public boolean hasChildren() {
		return ! childEntries.isEmpty();
	}

    /**
     *
     * @return the number of children
     */
	public int getChildCount() {
		return childEntries.size();
	}


    /**
     *
     * @return the root parent's root of this entry or self
     */
	public Entry getRoot() {
		return parent == null ? this : parent.getRoot();
	}

	@Override
	public String toString() {
		return "Entry [name=" + name + ", builders=" + builders + ", attributes=" + attributes + ", childEntries="
		        + childEntries + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((attributes == null) ? 0 : attributes.hashCode());
		result = prime * result + ((builders == null) ? 0 : builders.hashCode());
		result = prime * result + ((childEntries == null) ? 0 : childEntries.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null || getClass() != obj.getClass())
			return false;

		Entry other = (Entry) obj;

		return Objects.equals(attributes, other.attributes) && Objects.equals(builders, other.builders) && Objects.equals(childEntries, other.childEntries) && Objects.equals(name, other.name);
	}

    /**
     * Get a child by it's name
     * @param name the child name
     * @return the child corresponding to the name or null
     */
	public Entry getChild(String name) {
		for (Entry child : children()) {
	        final String childName = child.getName();
			if (childName.isEmpty()) {
				final Entry deepChild = child.getChild(name);
				if (deepChild != null)
					return deepChild;
			}
	        if (name.equals(childName))
				return child;
        }
		return null;
	}

    /**
     * Find en entry corresponding to the name
     * @param name the name of the entry to find
     * @return an object Entry if found else null
     */
	public Entry findEntry(String name) {
		if (this.name.equals(name))
			return this;
		for (Entry child : this.children()) {
			Entry entry = child.findEntry(name);
			if (entry != null)
				return entry;
		}
		return null;
	}

    /**
     * Find all entries corresponding to a given name
     * @param name the name of the entries to find
     * @return a List of Entry object
     */
	public List<Entry> findEntries(String name) {
		List<Entry> entries = new ArrayList<Entry>();
		if (this.name.equals(name))
			entries.add(this);
		for (Entry child : this.children()) {
			entries.addAll(child.findEntries(name));
		}
		return entries;
	}

	public boolean isLeaf() {
		return childEntries.isEmpty();
	}

    /**
     * Get a child by it's path
     * @param names the path value
     * @return an Entry object or null if not found
     */
	public Entry getChildByPath(String... names) {
		Entry entry = this;
		for (String name : names) {
			if (!name.isEmpty())
				entry = entry.getChild(name);
			if (entry == null)
				break;
		}
		return entry;
	}


    /**
     * Remove a given entry from childs
     * @param entry the entry to remove
     */
	public void remove(Entry entry) {
		childEntries.remove(entry);
	}
}
