/*
 *  Freeplane - mind map editor
 *  Copyright (C) 2011 dimitry
 *
 *  This file author is dimitry
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version CONTROL_SIZE of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.freeplane.core.ui.components.resizer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * @author Dimitry Polivaev
 * Jan 24, 2011
 */
@SuppressWarnings("serial")
public class JResizer extends JComponent {
    /**
     * La taille autorisée pour le resizer
     */
    private static final int CONTROL_SIZE = 5;

    /**
     * Un booléen pour le slider
     */
    protected boolean sliderLock = false;

    /**
     * Le point qui sera modifié au cours de l'application pour la souris
     */
    protected Point point;
    private int index;

    /**
     * Liste de resizerListeners
     */
    private final Set<ResizerListener> resizeListener = new LinkedHashSet<>();

    /**
     * Enumeration interne sur la direction du resizer
     */
    public enum Direction {
        RIGHT, LEFT, UP, DOWN;

        /**
         * Fonction pour créer une box selon l'énum
         * @return La box
         */
        public Box createBox() {
            switch (this) {
                case RIGHT:
                case LEFT:
                    return Box.createHorizontalBox();
                default:
                    return Box.createVerticalBox();
            }
        }

        /**
         * Fonction pour récupérer la taille préférentielle
         * @param component Le component qui contient les informations sur la taille préférentielle
         * @return La largeur ou la hauteur préférentielle
         */
        public int getPreferredSize(final Component component) {
            final Dimension preferredSize = component.getPreferredSize();
            switch (this) {
                case RIGHT:
                case LEFT:
                    return preferredSize.width;
                default:
                    return preferredSize.height;
            }
        }

        /**
         * Fonction pour set la taille préférentielle
         * @param component Le component dans lequel on va set la taille préférentielle
         * @param size La hauteur ou la largeur préférentielle
         */
        public void setPreferredSize(Component component, int size) {
            switch (this) {
                case RIGHT:
                case LEFT:
                    component.setPreferredSize(new Dimension(size, 1));
                    return;
                default:
                    component.setPreferredSize(new Dimension(1, size));
            }
        }

        /**
         * FOnction pour créer une box à partir du component redimensionné
         * @param resizedComponent Le component redimensionné
         * @return La box créée
         */
        public Box createBox(Component resizedComponent) {
            final Box box = createBox();
            final JResizer resizer = createResizer();
            switch (this) {
                case RIGHT:
                case DOWN:
                    box.add(resizer);
                    box.add(resizedComponent);
                    break;
                default:
                    box.add(resizedComponent);
                    box.add(resizer);
            }
            return box;

        }

        protected JResizer createResizer() {
            return new JResizer(this);
        }
    }

    /**
     * Constructeur prenant en paramètre la direction qu'aura le resize
     * @param direction La direction
     */
    public JResizer(final Direction direction) {
        setOpaque(true);
        final int width;
        final int height;

        /*
        On regarde la direction passée en paramètre pour set le curseur
         */
        if (direction.equals(Direction.RIGHT)) {
            width = CONTROL_SIZE;
            height = 0;
            setCursor(Cursor.getPredefinedCursor(Cursor.W_RESIZE_CURSOR));
        } else if (direction.equals(Direction.LEFT)) {
            height = CONTROL_SIZE;
            width = 0;
            setCursor(Cursor.getPredefinedCursor(Cursor.E_RESIZE_CURSOR));
        } else if (direction.equals(Direction.UP)) {
            height = 0;
            width = CONTROL_SIZE;
            setCursor(Cursor.getPredefinedCursor(Cursor.S_RESIZE_CURSOR));
        } else /*Direction.DOWN*/ {
            height = 0;
            width = CONTROL_SIZE;
            setCursor(Cursor.getPredefinedCursor(Cursor.N_RESIZE_CURSOR));
        }

        setPreferredSize(new Dimension(width, height));

        /*
        On set les mouse adapter à null
         */
        addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                point = null;
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                point = null;
            }
        });

        addMouseMotionListener(new MouseMotionAdapter() {
            private int getIndex() {
                final Container parent = getParent();
                for (int i = 0; i < parent.getComponentCount(); i++) {
                    if (!JResizer.this.equals(parent.getComponent(i)))
                        continue;
                    if (direction.equals(Direction.RIGHT) || direction.equals(Direction.DOWN)) {
                        return i + 1;
                    } else if (direction.equals(Direction.LEFT) || direction.equals(Direction.UP)) {
                        return i - 1;
                    }
                }
                return -1;
            }

            @Override
            public void mouseDragged(MouseEvent e) {
                if (sliderLock) return;
                final Point point2 = e.getPoint();
                SwingUtilities.convertPointToScreen(point2, e.getComponent());
                if (point != null) {
                    final JComponent parent = (JComponent) getParent();
                    final Component resizedComponent = parent.getComponent(index);
                    final Dimension size = new Dimension(resizedComponent.getPreferredSize());
                    if (direction.equals(Direction.RIGHT)) {
                        size.width -= (point2.x - point.x);
                    } else if (direction.equals(Direction.LEFT)) {
                        size.width += (point2.x - point.x);
                    } else if (direction.equals(Direction.UP)) {
                        size.height += (point2.y - point.y);
                    } else if (direction.equals(Direction.DOWN)) {
                        size.height -= (point2.y - point.y);
                    }
                    resizedComponent.setPreferredSize(new Dimension(Math.max(size.width, 0), Math.max(size.height, 0)));
                    parent.revalidate();
                    parent.repaint();
                    fireSizeChanged(resizedComponent);
                } else {
                    index = getIndex();
                }
                point = point2;
            }
        });
    }

    /**
     * Fonction pour créer un resizer listener sur le resizerlistener passé en paramètre
     * @param listener Le resizer listener qui aura l'action
     */
    public void addResizerListener(ResizerListener listener) {
        if (listener == null) return;

        synchronized (resizeListener) {
            resizeListener.add(listener);
        }

    }

    @SuppressWarnings("SuspiciousMethodCalls")
    public void removeResizerListener(ComponentListener listener) {
        if (listener == null) return;

        synchronized (resizeListener) {
            resizeListener.remove(listener);
        }
    }

    public void setSliderLocked(boolean enabled) {
        this.sliderLock = enabled;
    }

    public boolean isSliderLocked() {
        return this.sliderLock;
    }

    private void fireSizeChanged(Component resizedComponent) {
        ResizeEvent event = new ResizeEvent(this, resizedComponent);
        synchronized (this.resizeListener) {
            for (ResizerListener listener : resizeListener) {
                listener.componentResized(event);
            }
        }
    }
}
